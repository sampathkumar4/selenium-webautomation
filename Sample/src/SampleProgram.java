import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SampleProgram {
	
	public static void main(String[] args) throws InterruptedException
	{
		// declaration and instantiation of objects/variables
		WebDriver driver = new FirefoxDriver();
		
		String baseUrl = "http://demo.guru99.com/V4/index.php";
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// launch Firefox and direct it to the Base URL
		driver.get(baseUrl);
		Thread.sleep(4000);
		
		// Expected title of the page.
		String expectedTitle = "Guru99 Bank Home Page";

		// get the actual value of the title
		String actualTitle = driver.getTitle();

		// compare the actual title of the page with the expected one and print the result as "Passed" or "Failed"
		if (actualTitle.contentEquals(expectedTitle))
		{
			System.out.println("Test Passed!");
		}
		else
		{
			System.out.println("Test Failed");
			driver.quit();
		}

		// Enter UserID in UserID textbox..
		WebElement userIDTexbox = driver.findElement(By.name("uid"));
		userIDTexbox.sendKeys("mngr35771");
		Thread.sleep(4000);

		// Enter Password in Password textbox..
		WebElement passwordTextbox = driver.findElement(By.name("password"));
		passwordTextbox.sendKeys("YrUmenY");
		Thread.sleep(4000);

		// Click on Login button..
		WebElement loginButton = driver.findElement(By.name("btnLogin"));
		loginButton.click();
		Thread.sleep(4000);
		
		// Expected ManagerId..
		String expectedManagerId = "Manger Id : mngr35771";

		// Get the actual manager Id..
		WebElement managerId = driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[3]/td"));
		String actualManagerId = managerId.getText();

		// compare the Actual ManagerId of the page with the Expected ManagerId and print the result as "Passed" or "Failed"
		if (actualManagerId.contentEquals(expectedManagerId))
		{
			System.out.println("Test2 Passed!");
		}
		else 
		{
			System.out.println("Test2 Failed");
			driver.quit();
		}
		//close Firefox
		driver.quit();
		
	}

}
